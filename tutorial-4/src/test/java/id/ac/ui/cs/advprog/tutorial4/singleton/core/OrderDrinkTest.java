package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.graalvm.compiler.core.common.type.ArithmeticOpTable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderDrinkTest {
    private Class<?> orderDrinkClass;
    private OrderDrink orderDrink;

    @BeforeEach
    public void setUp() throws Exception {
        orderDrinkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink");
        orderDrink = OrderDrink.getInstance();
    }

    @Test
    public void testOrderDrinkHasPrivateConstructor() {
        List<Constructor> constructors = Arrays.asList(orderDrinkClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(constructor -> Modifier.isPrivate(constructor.getModifiers()))
        );
    }

    @Test
    public void testOrderDrinkExistAndIsNotNull() {
        assertNotNull(orderDrink);
    }

    @Test
    public void testOrderDrinkOnlyReturnOneInstance() {
        OrderDrink orderDrink1 = OrderDrink.getInstance();
        OrderDrink orderDrink2 = OrderDrink.getInstance();
        assertTrue(orderDrink1 == orderDrink2);
    }

    @Test
    public void testOrderDrinkReturnAccordingToSetDrink() {
        orderDrink.setDrink("Cocktail");
        assertEquals("Cocktail", orderDrink.getDrink());
    }
}