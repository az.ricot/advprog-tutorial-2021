package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import com.sun.tools.javac.comp.Flow;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class SnevnezhaShiratakiIngredientFactoryTest {
    private Class<?> snevnezhaShiratakiIngredientFactoryClass;

    private SnevnezhaShiratakiIngredientFactory snevnezhaShiratakiIngredientFactory;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiIngredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiIngredientFactory");
        snevnezhaShiratakiIngredientFactory = new SnevnezhaShiratakiIngredientFactory();
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(snevnezhaShiratakiIngredientFactoryClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryIsAIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideMethodCreateFlavor() throws Exception {
        Method createFlavor = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideMethodCreateMeat() throws Exception {
        Method createMeat = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideMethodCreateNoodle() throws Exception {
        Method createNoodle = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideMethodCreateTopping() throws Exception {
        Method createTopping = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryCreateFlavorReturnUmami() {
        assertTrue(snevnezhaShiratakiIngredientFactory.createFlavor() instanceof Umami);
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryCreateMeatReturnFish() {
        assertTrue(snevnezhaShiratakiIngredientFactory.createMeat() instanceof Fish);
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryCreateNoodleReturnShirataki() {
        assertTrue(snevnezhaShiratakiIngredientFactory.createNoodle() instanceof Shirataki);
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryCreateToppingReturnFlower() {
        assertTrue(snevnezhaShiratakiIngredientFactory.createTopping() instanceof Flower);
    }
}