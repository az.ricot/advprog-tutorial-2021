package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShiratakiTest {
    private Shirataki shirataki;

    @BeforeEach
    void setUp() {
        shirataki = new Shirataki();
    }

    @Test
    public void testShiratakiHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Snevnezha Shirataki Noodles...";
        assertEquals(shirataki.getDescription(), expectedString);
    }
}