package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SpicyTest {
    private Spicy spicy;

    @BeforeEach
    void setUp() {
        spicy = new Spicy();
    }

    @Test
    public void testSpicyHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Liyuan Chili Powder...";
        assertEquals(spicy.getDescription(), expectedString);
    }
}