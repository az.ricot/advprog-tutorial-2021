package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class MondoUdonIngredientFactoryTest {
    private Class<?> mondoUdonIngredientFactoryClass;

    private MondoUdonIngredientFactory mondoUdonIngredientFactory;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonIngredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonIngredientFactory");
        mondoUdonIngredientFactory = new MondoUdonIngredientFactory();
    }

    @Test
    public void testMondoUdonIngredientFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoUdonIngredientFactoryClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientFactoryIsAIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideMethodCreateFlavor() throws Exception {
        Method createFlavor = mondoUdonIngredientFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideMethodCreateMeat() throws Exception {
        Method createMeat = mondoUdonIngredientFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideMethodCreateNoodle() throws Exception {
        Method createNoodle = mondoUdonIngredientFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideMethodCreateTopping() throws Exception {
        Method createTopping = mondoUdonIngredientFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientFactoryCreateFlavorReturnSalty() {
        assertTrue(mondoUdonIngredientFactory.createFlavor() instanceof Salty);
    }

    @Test
    public void testMondoUdonIngredientFactoryCreateMeatReturnChicken() {
        assertTrue(mondoUdonIngredientFactory.createMeat() instanceof Chicken);
    }

    @Test
    public void testMondoUdonIngredientFactoryCreateNoodleReturnUdon() {
        assertTrue(mondoUdonIngredientFactory.createNoodle() instanceof Udon);
    }

    @Test
    public void testMondoUdonIngredientFactoryCreateToppingReturnCheese() {
        assertTrue(mondoUdonIngredientFactory.createTopping() instanceof Cheese);
    }
}