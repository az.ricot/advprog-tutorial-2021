package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheeseTest {
    private Cheese cheese;

    @BeforeEach
    void setUp() {
        cheese = new Cheese();
    }

    @Test
    public void testCheeseHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Shredded Cheese Topping...";
        assertEquals(cheese.getDescription(), expectedString);
    }
}