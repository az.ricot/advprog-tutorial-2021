package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SnevnezhaShiratakiTest {
    private SnevnezhaShirataki snevnezhaShirataki;

    @BeforeEach
    public void setup() throws Exception {
        snevnezhaShirataki = new SnevnezhaShirataki("Nama Shirataki");
    }

    @Test
    public void testGetName() {
        assertEquals(snevnezhaShirataki.getName(), "Nama Shirataki");
    }

    @Test
    public void testGetFlavorShouldReturnUmami() {
        assertTrue(snevnezhaShirataki.getFlavor() instanceof Umami);
    }

    @Test
    public void testGetMeatShouldReturnFish() {
        assertTrue(snevnezhaShirataki.getMeat() instanceof Fish);
    }

    @Test
    public void testGetNoodleShouldReturnShirataki() {
        assertTrue(snevnezhaShirataki.getNoodle() instanceof Shirataki);
    }

    @Test
    public void testGetToppingShouldReturnFlower() {
        assertTrue(snevnezhaShirataki.getTopping() instanceof Flower);
    }
}