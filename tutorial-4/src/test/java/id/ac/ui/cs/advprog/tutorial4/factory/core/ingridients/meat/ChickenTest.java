package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChickenTest {
    private Chicken chicken;

    @BeforeEach
    void setUp() {
        chicken = new Chicken();
    }

    @Test
    public void testChickenHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Wintervale Chicken Meat...";
        assertEquals(chicken.getDescription(), expectedString);
    }
}