package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class MenuTest {
    private Class<?> menuClass;

    @BeforeEach
    void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMenuIsAPublicAbstract() {
        int classModifiers = menuClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void testMenuHasGetNoodleConcreteMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getNoodle.getParameterCount());
    }

    @Test
    public void testMenuHasGetMeatConcreteMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getMeat.getParameterCount());
    }

    @Test
    public void testMenuHasGetToppingConcreteMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getTopping.getParameterCount());
    }

    @Test
    public void testMenuHasGetFlavorConcreteMethod() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getFlavor.getParameterCount());
    }

}