package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FlowerTest {
    private Flower flower;

    @BeforeEach
    void setUp() {
        flower = new Flower();
    }

    @Test
    public void testFlowerHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Xinqin Flower Topping...";
        assertEquals(flower.getDescription(), expectedString);
    }
}