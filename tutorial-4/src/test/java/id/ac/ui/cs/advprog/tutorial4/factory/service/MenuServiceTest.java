package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class MenuServiceTest {
    private Class<?> menuServiceClass;

    private MenuService menuService;

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceHasCreateMenuMethod() throws Exception {
        Method createMenu = menuServiceClass.getDeclaredMethod("createMenu", String.class, String.class);
        int methodModifiers = createMenu.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuServiceMethodGetMenusReturnListWithTheRightAmount() {
        assertEquals(menuService.getMenus().size(), 4);
    }

    @Test
    public void testMenuServiceCreateMenuLiyuanSoba() {
        String name = "1";
        Menu liyuanSoba = menuService.createMenu(name, "LiyuanSoba");
        assertTrue(liyuanSoba instanceof LiyuanSoba);
        assertEquals(liyuanSoba.getName(), name);
    }

    @Test
    public void testMenuServiceCreateMenuInuzumaRamen() {
        String name = "2";
        Menu inuzumaRamen = menuService.createMenu(name, "InuzumaRamen");
        assertTrue(inuzumaRamen instanceof InuzumaRamen);
        assertEquals(inuzumaRamen.getName(), name);
    }

    @Test
    public void testMenuServiceCreateMenuMondoUdon() {
        String name = "3";
        Menu mondoUdon = menuService.createMenu(name, "MondoUdon");
        assertTrue(mondoUdon instanceof MondoUdon);
        assertEquals(mondoUdon.getName(), name);
    }

    @Test
    public void testMenuServiceCreateMenuShirataki() {
        String name = "1";
        Menu shirataki = menuService.createMenu(name, "Shirataki");
        assertTrue(shirataki instanceof SnevnezhaShirataki);
        assertEquals(shirataki.getName(), name);
    }
}