package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderFoodTest {
    private Class<?> orderFoodClass;
    private OrderFood orderFood;

    @BeforeEach
    public void setUp() throws Exception {
        orderFoodClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood");
        orderFood = OrderFood.getInstance();
    }

    @Test
    public void testOrderFoodHasPrivateConstructor() {
        List<Constructor> constructors = Arrays.asList(orderFoodClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(constructor -> Modifier.isPrivate(constructor.getModifiers()))
        );
    }

    @Test
    public void testOrderFoodExistAndIsNotNull() {
        assertNotNull(orderFood);
    }

    @Test
    public void testOrderFoodOnlyReturnOneInstance() {
        OrderFood orderFood1 = OrderFood.getInstance();
        OrderFood orderFood2 = OrderFood.getInstance();
        assertTrue(orderFood1 == orderFood2);
    }

    @Test
    public void testOrderFoodReturnAccordingToSetFood() {
        orderFood.setFood("Nasi Goreng");
        assertEquals("Nasi Goreng", orderFood.getFood());
    }
}