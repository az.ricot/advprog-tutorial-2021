package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RamenTest {
    private Ramen ramen;

    @BeforeEach
    void setUp() {
        ramen = new Ramen();
    }

    @Test
    public void testRamenHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Inuzuma Ramen Noodles...";
        assertEquals(ramen.getDescription(), expectedString);
    }
}