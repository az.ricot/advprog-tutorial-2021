package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UmamiTest {
    private Umami umami;

    @BeforeEach
    void setUp() {
        umami = new Umami();
    }

    @Test
    public void testUmamiHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding WanPlus Specialty MSG flavoring...";
        assertEquals(umami.getDescription(), expectedString);
    }
}