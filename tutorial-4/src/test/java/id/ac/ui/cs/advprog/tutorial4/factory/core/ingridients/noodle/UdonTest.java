package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UdonTest {
    private Udon udon;

    @BeforeEach
    void setUp() {
        udon = new Udon();
    }

    @Test
    public void testUdonHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Mondo Udon Noodles...";
        assertEquals(udon.getDescription(), expectedString);
    }
}