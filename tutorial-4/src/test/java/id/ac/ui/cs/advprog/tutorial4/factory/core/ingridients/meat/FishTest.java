package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FishTest {
    private Fish fish;

    @BeforeEach
    void setUp() {
        fish = new Fish();
    }

    @Test
    public void testFishHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Zhangyun Salmon Fish Meat...";
        assertEquals(fish.getDescription(), expectedString);
    }
}