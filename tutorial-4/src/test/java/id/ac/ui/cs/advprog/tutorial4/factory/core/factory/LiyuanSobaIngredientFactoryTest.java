package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class LiyuanSobaIngredientFactoryTest {
    private Class<?> liyuanSobaIngredientFactoryClass;

    private LiyuanSobaIngredientFactory liyuanSobaIngredientFactory;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaIngredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaIngredientFactory");
        liyuanSobaIngredientFactory = new LiyuanSobaIngredientFactory();
    }

    @Test
    public void testLiyuanSobaIngredientFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(liyuanSobaIngredientFactoryClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryIsAIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideMethodCreateFlavor() throws Exception {
        Method createFlavor = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideMethodCreateMeat() throws Exception {
        Method createMeat = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideMethodCreateNoodle() throws Exception {
        Method createNoodle = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideMethodCreateTopping() throws Exception {
        Method createTopping = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryCreateFlavorReturnSweet() {
        assertTrue(liyuanSobaIngredientFactory.createFlavor() instanceof Sweet);
    }

    @Test
    public void testLiyuanSobaIngredientFactoryCreateMeatReturnBeef() {
        assertTrue(liyuanSobaIngredientFactory.createMeat() instanceof Beef);
    }

    @Test
    public void testLiyuanSobaIngredientFactoryCreateNoodleReturnSoba() {
        assertTrue(liyuanSobaIngredientFactory.createNoodle() instanceof Soba);
    }

    @Test
    public void testLiyuanSobaIngredientFactoryCreateToppingReturnMushroom() {
        assertTrue(liyuanSobaIngredientFactory.createTopping() instanceof Mushroom);
    }
}