package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class OrderServiceTest {
    private Class<?> orderServiceClass;
    private OrderService orderService;

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testOrderServiceHasGetDrinkMethod() throws Exception {
        Method getDrink = orderServiceClass.getDeclaredMethod("getDrink");
        int methodModifiers = getDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceHasGetFoodMethod() throws Exception {
        Method getFood = orderServiceClass.getDeclaredMethod("getFood");
        int methodModifiers = getFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceHasOrderADrinkMethod() throws Exception {
        Method orderADrink = orderServiceClass.getDeclaredMethod("orderADrink", String.class);
        int methodModifiers = orderADrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceHasOrderAFoodMethod() throws Exception {
        Method orderAFood = orderServiceClass.getDeclaredMethod("orderAFood", String.class);
        int methodModifiers = orderAFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceMethodOrderADrinkShouldUpdateDrink() {
        String name = "Cocktail";
        orderService.orderADrink(name);
        assertEquals(name, orderService.getDrink().toString());
    }

    @Test
    public void testOrderServiceMethodOrderAFoodShouldUpdateFood() {
        String name = "Nasi Goreng";
        orderService.orderAFood(name);
        assertEquals(name, orderService.getFood().toString());
    }
}