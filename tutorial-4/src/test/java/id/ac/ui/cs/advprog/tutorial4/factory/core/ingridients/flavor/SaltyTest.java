package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SaltyTest {
    private Salty salty;

    @BeforeEach
    void setUp() {
        salty = new Salty();
    }

    @Test
    public void testSaltyHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding a pinch of salt...";
        assertEquals(salty.getDescription(), expectedString);
    }
}