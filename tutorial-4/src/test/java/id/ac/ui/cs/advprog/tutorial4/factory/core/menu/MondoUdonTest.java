package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MondoUdonTest {
    private MondoUdon mondoUdon;

    @BeforeEach
    public void setup() throws Exception {
        mondoUdon = new MondoUdon("Nama Udon");
    }

    @Test
    public void testGetName() {
        assertEquals(mondoUdon.getName(), "Nama Udon");
    }

    @Test
    public void testGetFlavorShouldReturnSalty() {
        assertTrue(mondoUdon.getFlavor() instanceof Salty);
    }

    @Test
    public void testGetMeatShouldReturnChicken() {
        assertTrue(mondoUdon.getMeat() instanceof Chicken);
    }

    @Test
    public void testGetNoodleShouldReturnUdon() {
        assertTrue(mondoUdon.getNoodle() instanceof Udon);
    }

    @Test
    public void testGetToppingShouldReturnCheese() {
        assertTrue(mondoUdon.getTopping() instanceof Cheese);
    }
}