package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MushroomTest {
    private Mushroom mushroom;

    @BeforeEach
    void setUp() {
        mushroom = new Mushroom();
    }

    @Test
    public void testMushroomHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Shiitake Mushroom Topping...";
        assertEquals(mushroom.getDescription(), expectedString);
    }
}