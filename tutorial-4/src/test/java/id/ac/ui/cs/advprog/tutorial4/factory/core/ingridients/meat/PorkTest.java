package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PorkTest {
    private Pork pork;

    @BeforeEach
    void setUp() {
        pork = new Pork();
    }

    @Test
    public void testPorkHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Tian Xu Pork Meat...";
        assertEquals(pork.getDescription(), expectedString);
    }
}