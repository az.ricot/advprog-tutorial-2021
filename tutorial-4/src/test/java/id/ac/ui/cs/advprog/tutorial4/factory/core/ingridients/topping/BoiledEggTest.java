package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoiledEggTest {
    private BoiledEgg boiledEgg;

    @BeforeEach
    void setUp() {
        boiledEgg = new BoiledEgg();
    }

    @Test
    public void testBoiledEggHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Guahuan Boiled Egg Topping";
        assertEquals(boiledEgg.getDescription(), expectedString);
    }
}