package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SweetTest {
    private Sweet sweet;

    @BeforeEach
    void setUp() {
        sweet = new Sweet();
    }

    @Test
    public void testSweetHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding a dash of Sweet Soy Sauce...";
        assertEquals(sweet.getDescription(), expectedString);
    }
}