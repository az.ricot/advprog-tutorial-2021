package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InuzumaRamenTest {
    private InuzumaRamen inuzumaRamen;

    @BeforeEach
    public void setup() throws Exception {
        inuzumaRamen = new InuzumaRamen("Nama Ramen");
    }

    @Test
    public void testGetName() {
        assertEquals(inuzumaRamen.getName(), "Nama Ramen");
    }

    @Test
    public void testGetFlavorShouldReturnSpicy() {
        assertTrue(inuzumaRamen.getFlavor() instanceof Spicy);
    }

    @Test
    public void testGetMeatShouldReturnPork() {
        assertTrue(inuzumaRamen.getMeat() instanceof Pork);
    }

    @Test
    public void testGetNoodleShouldReturnRamen() {
        assertTrue(inuzumaRamen.getNoodle() instanceof Ramen);
    }

    @Test
    public void testGetToppingShouldReturnBoiledEgg() {
        assertTrue(inuzumaRamen.getTopping() instanceof BoiledEgg);
    }
}