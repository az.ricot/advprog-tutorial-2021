package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class InuzumaRamenIngredientFactoryTest {
    private Class<?> inuzamiRamenIngredientFactoryClass;

    private InuzumaRamenIngredientFactory inuzumaRamenIngredientFactory;

    @BeforeEach
    public void setUp() throws Exception {
        inuzamiRamenIngredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenIngredientFactory");
        inuzumaRamenIngredientFactory = new InuzumaRamenIngredientFactory();
    }

    @Test
    public void testInuzamiRamenIngredientFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(inuzamiRamenIngredientFactoryClass.getModifiers()));
    }

    @Test
    public void testInuzamiRamenIngredientFactoryIsAIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzamiRamenIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testInuzamiRamenIngredientFactoryOverrideMethodCreateFlavor() throws Exception {
        Method createFlavor = inuzamiRamenIngredientFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testInuzamiRamenIngredientFactoryOverrideMethodCreateMeat() throws Exception {
        Method createMeat = inuzamiRamenIngredientFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testInuzamiRamenIngredientFactoryOverrideMethodCreateNoodle() throws Exception {
        Method createNoodle = inuzamiRamenIngredientFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testInuzamiRamenIngredientFactoryOverrideMethodCreateTopping() throws Exception {
        Method createTopping = inuzamiRamenIngredientFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testInuzamiRamenIngredientFactoryCreateFlavorReturnSpicy() {
        assertTrue(inuzumaRamenIngredientFactory.createFlavor() instanceof Spicy);
    }

    @Test
    public void testInuzamiRamenIngredientFactoryCreateMeatReturnPork() {
        assertTrue(inuzumaRamenIngredientFactory.createMeat() instanceof Pork);
    }

    @Test
    public void testInuzamiRamenIngredientFactoryCreateNoodleReturnRamen() {
        assertTrue(inuzumaRamenIngredientFactory.createNoodle() instanceof Ramen);
    }

    @Test
    public void testInuzamiRamenIngredientFactoryCreateToppingReturnBoiledEgg() {
        assertTrue(inuzumaRamenIngredientFactory.createTopping() instanceof BoiledEgg);
    }
}
