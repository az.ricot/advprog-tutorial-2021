package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class SobaTest {
    private Soba soba;

    @BeforeEach
    void setUp() {
        soba = new Soba();
    }

    @Test
    public void testSobaHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Liyuan Soba Noodles...";
        assertEquals(soba.getDescription(), expectedString);
    }
}