package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @BeforeEach
    public void setup() {
        menuRepository = new MenuRepository();
        menuRepository.add(new InuzumaRamen("1"));
        menuRepository.add(new LiyuanSoba("2"));
        menuRepository.add(new MondoUdon("3"));
        menuRepository.add(new SnevnezhaShirataki("4"));
    }

    @Test
    public void testMenuRepositoryMethodGetMenusShouldReturnListWithCorrectAmount() {
        assertEquals(menuRepository.getMenus().size(), 4);
    }

    @Test
    public void testMenuRepositoryMethodAddShouldAddToListOfMenuRepository() {
        int sizeNow = menuRepository.getMenus().size();

        menuRepository.add(new LiyuanSoba("5"));
        assertEquals(sizeNow + 1, menuRepository.getMenus().size());
        sizeNow = menuRepository.getMenus().size();

        menuRepository.add(new SnevnezhaShirataki("6"));
        assertEquals(sizeNow + 1, menuRepository.getMenus().size());
    }
}