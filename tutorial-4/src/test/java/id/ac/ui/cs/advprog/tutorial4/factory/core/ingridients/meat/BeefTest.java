package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BeefTest {
    private Beef beef;

    @BeforeEach
    void setUp() {
        beef = new Beef();
    }

    @Test
    public void testBeefHasMethodGetDescriptionAndReturnExpectedString() {
        String expectedString = "Adding Maro Beef Meat...";
        assertEquals(beef.getDescription(), expectedString);
    }
}