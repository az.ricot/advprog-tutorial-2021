package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LiyuanSobaTest {
    private LiyuanSoba liyuanSoba;

    @BeforeEach
    public void setup() throws Exception {
        liyuanSoba = new LiyuanSoba("Nama Soba");
    }

    @Test
    public void testGetName() {
        assertEquals(liyuanSoba.getName(), "Nama Soba");
    }

    @Test
    public void testGetFlavorShouldReturnSweet() {
        assertTrue(liyuanSoba.getFlavor() instanceof Sweet);
    }

    @Test
    public void testGetMeatShouldReturnBeef() {
        assertTrue(liyuanSoba.getMeat() instanceof Beef);
    }

    @Test
    public void testGetNoodleShouldReturnSoba() {
        assertTrue(liyuanSoba.getNoodle() instanceof Soba);
    }

    @Test
    public void testGetToppingShouldReturnMushroom() {
        assertTrue(liyuanSoba.getTopping() instanceof Mushroom);
    }
}