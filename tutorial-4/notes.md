## Perbedaan dari Lazy Instantiation dan Eager Instantiation

**Lazy Instantiation** akan menutup kesempatan untuk membuat objek jika objek tersebut sudah pernah terbuat dulunya. Dalam artian lain, Lazy Instantiation akan selalu mengecek apakah objek tersebut sudah terbuat atau belum setiap terjadi pemanggilan/permintaan objek tersebut. Jika belum ada objek tersebut sebelumnya (masih null), maka akan dibuat objeknya. Hal ini memastikan hanya ada 1 objek yang dapat dibuat pada single thread.

Berbeda dengan Lazy Instantiation, **Eager Instantiation** akan langsung membuat objek di awal mula program berjalan (Tidak ada public constructor). Oleh karena itu, dipakai atau tidak, objek tersebut tetap akan terbuat dan hanya terbuat 1 kali saja. 

### Kelebihan dan Kekurangan Lazy Instantiation
Kelebihan dari Lazy Instantiation adalah objek akan dibuat jika dibutuhkan saja. Jika objek ternyata tidak dipakai, objek tidak akan dibuat dengan sia-sia. Selain itu, waktu startup program juga akan lebih cepat karena saat startup tidak perlu membuat objek. Sedangkan kekurangan dari Lazy Instantiation adalah saat objek dipanggil oleh beberapa client dalam waktu yang dekat (dalam multithreading), maka bisa saja pengecekan null menghasilkan hasil yang salah. Dapat ditambahkan Synchronized pada method namun akan menggunakan resources yang cukup banyak.

### Kelebihan dan Kekurangan Eager Instantiation
Kelebihan dari Eager Instantiation adalah tidak mungkin terjadi kesalahan saat multithreading seperti pada Lazy Instantiation. Kekurangannya adalah saat objek yang dibuat tersebut tidak pernah digunakan. Karena saat objek tidak digunakan, maka sia-sia memory yang telah digunakan dan resources/waktu yang digunakan saat startup.