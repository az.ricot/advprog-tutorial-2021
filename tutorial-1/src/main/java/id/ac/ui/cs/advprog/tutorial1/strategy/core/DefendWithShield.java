package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String defend() {
        return "Pegel tangan ane..";
    }

    @Override
    public String getType() {
        return "shield";
    }
}
