package id.ac.ui.cs.advprog.tutorial1.strategy.core;

import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer() {
        this.setAttackBehavior(new AttackWithGun());
        this.setDefenseBehavior(new DefendWithBarrier());
    }

    @Override
    public String getAlias() {
        return "Agile Adventurer";
    }
