package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class FacadeOfTransformationTest {
    private Class<?> facadeClass;

    @InjectMocks
    private FacadeOfTransformation facade;

    @BeforeEach
    public void setup() throws Exception {
        facadeClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.FacadeOfTransformation");
    }

    @Test
    public void testFacadeOfTransformationIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(facadeClass.getModifiers()));
    }

    @Test
    public void testFacadeOfTransformationIsATransformation() {
        Collection<Type> interfaces = Arrays.asList(facadeClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation")));
    }

    @Test
    public void testFacadeOfTransformationOverrideEncodeMethod() throws Exception {
        Class<?>[] encodeArgs = new Class[1];
        encodeArgs[0] = Spell.class;
        Method encode = facadeClass.getDeclaredMethod("encode", encodeArgs);

        assertEquals("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell",
                encode.getGenericReturnType().getTypeName());
        assertEquals(1,
                encode.getParameterCount());
        assertTrue(Modifier.isPublic(encode.getModifiers()));
    }

    @Test
    public void testFacadeOfTransformationEncodeCorrectlyImplemented() {
        Spell spell = new Spell("Soal akan dibagikan dalam bentuk tar gz asc", AlphaCodex.getInstance());
        String result = facade.encode(spell).getText();
        assertEquals(";<D>MaGD]qXSG_=!FF**]*!)wZD]B)>H!!;(^b>^|JM", result);
    }

    @Test
    public void testFacadeOfTransformationOverrideDecodeMethod() throws Exception {
        Class<?>[] decodeArgs = new Class[1];
        decodeArgs[0] = Spell.class;
        Method decode = facadeClass.getDeclaredMethod("decode", decodeArgs);

        assertEquals("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell",
                decode.getGenericReturnType().getTypeName());
        assertEquals(1,
                decode.getParameterCount());
        assertTrue(Modifier.isPublic(decode.getModifiers()));
    }

    @Test
    public void testFacadeOfTransformationDecodeCorrectlyImplemented() {
        Spell spell = new Spell(";<D>MaGD]qXSG_=!FF**]*!)wZD]B)>H!!;(^b>^|JM", RunicCodex.getInstance());
        String result = facade.decode(spell).getText();
        assertEquals("Soal akan dibagikan dalam bentuk tar gz asc", result);
    }

}