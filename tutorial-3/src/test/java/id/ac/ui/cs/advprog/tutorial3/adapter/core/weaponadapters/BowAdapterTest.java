package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.IonicBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.UranosBow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

// TODO: add tests
@ExtendWith(MockitoExtension.class)
public class BowAdapterTest {
    private Class<?> bowAdapterClass;
    private Class<?> bowClass;

    private BowAdapter bowAdapter;

    @Mock
    private Bow bow;

    @BeforeEach
    public void setUp() throws Exception {
        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");
        bowAdapter = new BowAdapter(bow);
    }

    @Test
    public void testBowAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(bowAdapterClass.getModifiers()));
    }

    @Test
    public void testBowAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(bowAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testBowAdapterConstructorReceivesBowAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = bowClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                bowAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testBowAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = bowAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = bowAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetNameMethod() throws Exception {
        Method getName = bowAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = bowAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method

    // Ionic Bow
    @Test
    public void testBowAdapterForIonicBowNormalAttackInSpontaneousMode() throws Exception {
        IonicBow ionicBow = new IonicBow("testBow");

        when(bow.shootArrow(false)).thenReturn(ionicBow.shootArrow(false));
        assertEquals(bowAdapter.normalAttack(), ionicBow.shootArrow(false));
    }

    @Test
    public void testBowAdapterForIonicBowChargedAttackToSwitchModeToAimShotModeAndNormalAttackInThisMode() throws Exception {
        IonicBow ionicBow = new IonicBow("testBow");

        when(bow.shootArrow(true)).thenReturn(ionicBow.shootArrow(true));
        String str = bowAdapter.chargedAttack();
        assertEquals(str, "Enter aim shot mode!");
        assertEquals(bowAdapter.normalAttack(), ionicBow.shootArrow(true));
    }

    @Test
    public void testBowAdapterForIonicBowChargedAttackToSwitchBackToSpontaneousModeAndNormalAttackInThisMode() throws Exception {
        IonicBow ionicBow = new IonicBow("testBow");

        when(bow.shootArrow(false)).thenReturn(ionicBow.shootArrow(false));
        bowAdapter.chargedAttack();
        String str = bowAdapter.chargedAttack();
        assertEquals(str, "Enter spontaneous mode!");
        assertEquals(bowAdapter.normalAttack(), ionicBow.shootArrow(false));
    }

    @Test
    public void testBowAdapterForIonicBowGetNameMethod() throws Exception {
        IonicBow ionicBow = new IonicBow("testBow");

        when(bow.getName()).thenReturn(ionicBow.getName());
        assertEquals(bowAdapter.getName(), ionicBow.getName());
    }

    @Test
    public void testBowAdapterForIonicBowGetHolderNameMethod() throws Exception {
        IonicBow ionicBow = new IonicBow("testBow");

        when(bow.getHolderName()).thenReturn(ionicBow.getHolderName());
        assertEquals(bowAdapter.getHolderName(), ionicBow.getHolderName());
    }

    // Uranos Bow
    @Test
    public void testBowAdapterForUranosBowNormalAttackInSpontaneousMode() throws Exception {
        UranosBow uranosBow = new UranosBow("testBow");

        when(bow.shootArrow(false)).thenReturn(uranosBow.shootArrow(false));
        assertEquals(bowAdapter.normalAttack(), uranosBow.shootArrow(false));
    }

    @Test
    public void testBowAdapterForUranosBowChargedAttackToSwitchModeToAimShotModeAndNormalAttackInThisMode() throws Exception {
        UranosBow uranosBow = new UranosBow("testBow");

        when(bow.shootArrow(true)).thenReturn(uranosBow.shootArrow(true));
        String str = bowAdapter.chargedAttack();
        assertEquals(str, "Enter aim shot mode!");
        assertEquals(bowAdapter.normalAttack(), uranosBow.shootArrow(true));
    }

    @Test
    public void testBowAdapterForUranosBowChargedAttackToSwitchBackToSpontaneousModeAndNormalAttackInThisMode() throws Exception {
        UranosBow uranosBow = new UranosBow("testBow");

        when(bow.shootArrow(false)).thenReturn(uranosBow.shootArrow(false));
        bowAdapter.chargedAttack();
        String str = bowAdapter.chargedAttack();
        assertEquals(str, "Enter spontaneous mode!");
        assertEquals(bowAdapter.normalAttack(), uranosBow.shootArrow(false));
    }

    @Test
    public void testBowAdapterForUranosBowGetNameMethod() throws Exception {
        UranosBow uranosBow = new UranosBow("testBow");

        when(bow.getName()).thenReturn(uranosBow.getName());
        assertEquals(bowAdapter.getName(), uranosBow.getName());
    }

    @Test
    public void testBowAdapterForUranosBowGetHolderNameMethod() throws Exception {
        UranosBow uranosBow = new UranosBow("testBow");

        when(bow.getHolderName()).thenReturn(uranosBow.getHolderName());
        assertEquals(bowAdapter.getHolderName(), uranosBow.getHolderName());
    }
}
