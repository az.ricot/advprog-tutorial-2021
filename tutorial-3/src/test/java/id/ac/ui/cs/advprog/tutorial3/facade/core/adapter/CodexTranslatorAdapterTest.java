package id.ac.ui.cs.advprog.tutorial3.facade.core.adapter;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Heatbearer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CodexTranslatorAdapterTest {
    private Class<?> codexTranslatorAdapterClass;
    private Class<?> codexTranslatorClass;

    private CodexTranslatorAdapter codexTranslatorAdapter;

    @Mock
    private CodexTranslator codexTranslator;

    @BeforeEach
    public void setUp() throws Exception {
        codexTranslatorAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.adapter.CodexTranslatorAdapter");
        codexTranslatorClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator");
        codexTranslatorAdapter = new CodexTranslatorAdapter();
    }

    @Test
    public void testCodexTranslatorAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(codexTranslatorAdapterClass.getModifiers()));
    }

    @Test
    public void testCodexTranslatorAdapterIsATransformation() {
        Collection<Type> interfaces = Arrays.asList(codexTranslatorAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation")));
    }

    @Test
    public void testCodexTranslatorAdapterOverrideEncodeMethod() throws Exception {
        Class<?>[] encodeArgs = new Class[1];
        encodeArgs[0] = Spell.class;
        Method encode = codexTranslatorAdapterClass.getDeclaredMethod("encode", encodeArgs);

        assertEquals("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell",
                encode.getGenericReturnType().getTypeName());
        assertEquals(1,
                encode.getParameterCount());
        assertTrue(Modifier.isPublic(encode.getModifiers()));
    }

    @Test
    public void testCodexTranslatorAdapterOverrideDecodeMethod() throws Exception {
        Class<?>[] decodeArgs = new Class[1];
        decodeArgs[0] = Spell.class;
        Method decode = codexTranslatorAdapterClass.getDeclaredMethod("decode", decodeArgs);

        assertEquals("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell",
                decode.getGenericReturnType().getTypeName());
        assertEquals(1,
                decode.getParameterCount());
        assertTrue(Modifier.isPublic(decode.getModifiers()));
    }

    @Test
    public void testCodexTranslatorAdapterForCodexTranslatorEncodingCallsTranslateWithRunixCodex() throws Exception {
        CodexTranslatorAdapter codexTranslatorAdapter = new CodexTranslatorAdapter();
        Spell spell = new Spell("Testing", AlphaCodex.getInstance());

        assertEquals(codexTranslatorAdapter.encode(spell).getText(), CodexTranslator.translate(spell, RunicCodex.getInstance()).getText());
    }

    @Test
    public void testCodexTranslatorAdapterForCodexTranslatorDecodingCallsTranslateWithAlphaCodex() throws Exception {
        CodexTranslatorAdapter codexTranslatorAdapter = new CodexTranslatorAdapter();
        Spell spell = new Spell("]?H!BsX", RunicCodex.getInstance());

        assertEquals(codexTranslatorAdapter.decode(spell).getText(), CodexTranslator.translate(spell, AlphaCodex.getInstance()).getText());
    }
}