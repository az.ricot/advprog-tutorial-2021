package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Heatbearer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.TheWindjedi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

// TODO: add tests
@ExtendWith(MockitoExtension.class)
public class SpellbookAdapterTest {
    private Class<?> spellbookAdapterClass;
    private Class<?> spellbookClass;

    private SpellbookAdapter spellbookAdapter;

    @Mock
    private Spellbook spellbook;

    @BeforeEach
    public void setUp() throws Exception {
        spellbookAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter");
        spellbookClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook");
        spellbookAdapter = new SpellbookAdapter(spellbook);
    }

    @Test
    public void testSpellbookAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(spellbookAdapterClass.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(spellbookAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testSpellbookAdapterConstructorReceivesSpellbookAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = spellbookClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                spellbookAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testSpellbookAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = spellbookAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = spellbookAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetNameMethod() throws Exception {
        Method getName = spellbookAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = spellbookAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method

    // Heat Bearer
    @Test
    public void testSpellAdapterForHeatbearerNormalAttackCallSmallSpell() throws Exception {
        Heatbearer heatbearer = new Heatbearer("testSpellbook");

        when(spellbook.smallSpell()).thenReturn(heatbearer.smallSpell());
        assertEquals(spellbookAdapter.normalAttack(), heatbearer.smallSpell());
    }

    @Test
    public void testSpellAdapterForHeatbearerChargedAttackCallLargeSpell() throws Exception {
        Heatbearer heatbearer = new Heatbearer("testSpellbook");

        when(spellbook.largeSpell()).thenReturn(heatbearer.largeSpell());
        assertEquals(spellbookAdapter.chargedAttack(), heatbearer.largeSpell());
    }

    @Test
    public void testSpellAdapterForHeatbearerCannotCallChargedAttackTwiceInARowButStillCanAfterTheFailedCall() throws Exception {
        Heatbearer heatbearer = new Heatbearer("testSpellbook");

        when(spellbook.largeSpell()).thenReturn(heatbearer.largeSpell());

        spellbookAdapter.chargedAttack();
        String str = spellbookAdapter.chargedAttack();
        String expected = "Magic power is not enough for a large spell";
        assertEquals(str, expected);

        assertEquals(spellbookAdapter.chargedAttack(), heatbearer.largeSpell());
    }

    @Test
    public void testSpellAdapterForHeatbearerGetNameMethod() throws Exception {
        Heatbearer heatbearer = new Heatbearer("testSpellbook");

        when(spellbook.getName()).thenReturn(heatbearer.getName());
        assertEquals(spellbookAdapter.getName(), heatbearer.getName());
    }

    @Test
    public void testSpellAdapterForHeatbearerGetHolderNameMethod() throws Exception {
        Heatbearer heatbearer = new Heatbearer("testSpellbook");

        when(spellbook.getHolderName()).thenReturn(heatbearer.getHolderName());
        assertEquals(spellbookAdapter.getHolderName(), heatbearer.getHolderName());
    }

    // The Wind Jedi
    @Test
    public void testSpellAdapterForTheWindJediNormalAttackCallSmallSpell() throws Exception {
        TheWindjedi theWindjedi = new TheWindjedi("testSpellbook");

        when(spellbook.smallSpell()).thenReturn(theWindjedi.smallSpell());
        assertEquals(spellbookAdapter.normalAttack(), theWindjedi.smallSpell());
    }

    @Test
    public void testSpellAdapterForTheWindJediChargedAttackCallLargeSpell() throws Exception {
        TheWindjedi theWindjedi = new TheWindjedi("testSpellbook");

        when(spellbook.largeSpell()).thenReturn(theWindjedi.largeSpell());
        assertEquals(spellbookAdapter.chargedAttack(), theWindjedi.largeSpell());
    }

    @Test
    public void testSpellAdapterForTheWindJediCannotCallChargedAttackTwiceInARowButStillCanAfterTheFailedCall() throws Exception {
        TheWindjedi theWindjedi = new TheWindjedi("testSpellbook");

        when(spellbook.largeSpell()).thenReturn(theWindjedi.largeSpell());

        spellbookAdapter.chargedAttack();
        String str = spellbookAdapter.chargedAttack();
        String expected = "Magic power is not enough for a large spell";
        assertEquals(str, expected);

        assertEquals(spellbookAdapter.chargedAttack(), theWindjedi.largeSpell());
    }

    @Test
    public void testSpellAdapterForTheWindJedirGetNameMethod() throws Exception {
        TheWindjedi theWindjedi = new TheWindjedi("testSpellbook");

        when(spellbook.getName()).thenReturn(theWindjedi.getName());
        assertEquals(spellbookAdapter.getName(), theWindjedi.getName());
    }

    @Test
    public void testSpellAdapterForTheWindJediGetHolderNameMethod() throws Exception {
        TheWindjedi theWindjedi = new TheWindjedi("testSpellbook");

        when(spellbook.getHolderName()).thenReturn(theWindjedi.getHolderName());
        assertEquals(spellbookAdapter.getHolderName(), theWindjedi.getHolderName());
    }
}
