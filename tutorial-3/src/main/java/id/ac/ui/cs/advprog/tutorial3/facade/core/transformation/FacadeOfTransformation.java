package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.adapter.CodexTranslatorAdapter;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.ArrayList;
import java.util.List;

public class FacadeOfTransformation implements Transformation {

    private List<Transformation> transformations;

    public FacadeOfTransformation() {
        this.transformations = new ArrayList<Transformation>();

        //current transformation that is needed
        this.addTransformation(new CelestialTransformation());
        this.addTransformation(new AbyssalTransformation());
        this.addTransformation(new CodexTranslatorAdapter());
        this.addTransformation(new CaesarTransformation());
    }

    @Override
    public Spell encode(Spell spell) {
        for (int i = 0; i < this.transformations.size(); i++) {
            spell = transformations.get(i).encode(spell);
        }
        return spell;
    }

    @Override
    public Spell decode(Spell spell) {
        for (int i = this.transformations.size() - 1; i >= 0; i--) {
            spell = transformations.get(i).decode(spell);
        }
        return spell;
    }

    //to add more transformation (not hardcoded)
    public void addTransformation(Transformation transformation) {
        this.transformations.add(transformation);
    }
}
