package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isExhausted;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.isExhausted = false;
    }

    @Override
    public String normalAttack() {
        this.isExhausted = false;
        return this.spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!this.isExhausted) {
            this.isExhausted = true;
            return this.spellbook.largeSpell();
        } else {
            this.isExhausted = false;
            return "Magic power is not enough for a large spell";
        }

    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }

}
