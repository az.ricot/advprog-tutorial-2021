package id.ac.ui.cs.advprog.tutorial3.facade.core.adapter;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;
import org.springframework.beans.factory.annotation.Autowired;

public class CodexTranslatorAdapter implements Transformation {

    @Override
    public Spell encode(Spell spell) {
        return CodexTranslator.translate(spell, RunicCodex.getInstance());
    }

    @Override
    public Spell decode(Spell spell) {
        return CodexTranslator.translate(spell, AlphaCodex.getInstance());
    }
}
